# Ruleta
#### Comentario

La solución presentada puedo haber sido más simple en algunos sentidos y más compleja en otros. En general, se trató de desarrollar una solución que cumpliera con el mínimo de todas las funcionales, sin agregar otras funcionalidades extras, que si bien hubieran sido un gran aporte para la usabilidad de la aplicación, se descartaron por razones de tiempo. Por otro lado, lo que corresponde al modelo de datos del problema, al código mismo y en general a las buenas prácticas y convenciones de desarrollo de aplicaciones RoR, se hizo un trabajo más extenso que lo necesario para llegar a la solución.

#####Supuestos y consideraciones:

  -  La solución se pudo haber logrado con un modelo de datos más simple, pero siempre es mejor modelar una solución escalable, con módulos que permitan alta cohesión y bajo acoplamiento, ya que de este modo, es mucho más "fácil" continuar con crecimiento de la aplicación y sus funcionalidades.
  -  No se realizó el desarrollo de vistas no especificadas en las instrucciones, pero se dejó toda la estructura necesaria lista para implementarlas.
  -  Se tomó el supuesto de que la aplicación es ejecutada por un solo usuario al mismo tiempo, para evitar problemas de concurrencia que podrían ocurrir por el hecho de que los "players" son globales. Eso se podría solucionar creando usuario únicos, por ejemplo con sesión.
  -  Las "rondas" que se generan cada 3 minutos son llamadas por ajax en un loop en una función de javascript. Si bien, podría haber sido mejor práctica hacer esas llamadas desde el lado del servidor, no si hizo por simplicidad y porque se tomó la suposición de que la simulación quiere ser vista "en vivo" por el usuario que realiza la simulación.
  -  Las llamadas a la api se realizan cada 1 hora, una vez comenzada la simulación.
  -  Las tablas "board" y "box" son creadas una sola vez (db rake:seed), por simplicidad del problema. Las tablas se crearon de todas formas por lo explicado en el punto 1.

##### Instrucciones

  -  Las vistas correspondientes que deben ser visitadas (ya que son parte integral de la solución) son:
	1. home/index: vista principal donde ocurre casi todo el flujo, y se visualiza la simulación.
	2. game/...: vistas para crear/ver/editar/eliminar partidas.
	3. player/...: vistas para crear/ver/editar/eliminar jugadores.
  -  Desde cualquier vista se puede volver a la vista principal haciendo click en el logo Nnodes.
  -  La simulación puede ser corrida eligiendo una partida y todos los jugadores necesarios que se quiera incluir.
  -  La simulación comienza al hacer click en 'comenzar simulación' y puede ser detenida haciendo click en 'detener simulación'.
  -  Una vez que la simulación comienza, se irán ejecutando nuevas rondas cada 3 minutos. Los resultados de dichas rondas, se irán desplegando en la tabla de la simulación en tiempo real.
  -  Cualquier pregunta no dude en consultar a icfuenza@gmail.com!