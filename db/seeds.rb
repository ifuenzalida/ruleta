# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


BOXES = []
BOARD = Board.new

if Board.count == 0
	BOARD.save
else
	BOARD = Board.first
end

if Box.count == 0
	verde = Box.new(color: 'verde', payoff: 15, board_id: BOARD.id)
	verde.save
	blanco = Box.new(color: 'blanco', payoff: 2, board_id: BOARD.id)
	blanco.save
	negro = Box.new(color: 'negro', payoff: 2, board_id: BOARD.id)
	negro.save

	BOXES << verde
	BOXES << blanco
	BOXES << negro
else
	BOXES = Box.all
end

