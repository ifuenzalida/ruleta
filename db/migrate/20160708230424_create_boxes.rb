class CreateBoxes < ActiveRecord::Migration
  def change
    create_table :boxes do |t|
      t.string :color
      t.integer :payoff

      t.timestamps null: false
    end
  end
end
