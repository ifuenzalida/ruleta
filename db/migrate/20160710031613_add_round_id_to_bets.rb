class AddRoundIdToBets < ActiveRecord::Migration
  def change
    add_column :bets, :round_id, :integer
  end
end
