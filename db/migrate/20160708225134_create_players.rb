class CreatePlayers < ActiveRecord::Migration
  def change
    create_table :players do |t|
      t.string :user_name
      t.integer :credit

      t.timestamps null: false
    end
  end
end
