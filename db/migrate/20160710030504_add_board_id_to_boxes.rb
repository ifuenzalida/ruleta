class AddBoardIdToBoxes < ActiveRecord::Migration
  def change
    add_column :boxes, :board_id, :integer
  end
end
