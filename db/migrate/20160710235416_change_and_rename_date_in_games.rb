class ChangeAndRenameDateInGames < ActiveRecord::Migration
  def change
  	rename_column :games, :date, :name
  	change_column :games, :name, :string
  end
end
