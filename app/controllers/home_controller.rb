class HomeController < ApplicationController
  def index
	  @games = Game.all ## entregar solo los q no tienen rondas hechas
  end

  def simulation_table
    @game = Game.find(params[:game_id])
    @players = []

    params[:player_ids].each do |player_id|
      @players << Player.find(player_id)
    end
  end

  def simulation
  	@game = Game.find(params[:game_id])
  	@players = []
  	@bets = []
  	@rounds = []
    @round_num = params[:round_num]
    @BOARD = Board.first

  	params[:player_ids].each do |player_id|
  		@players << Player.find(player_id)
  	end

  	# i = 0
  	# while i < 5 ##
  	# 	i = i +1
  		if Time.now.hour == 0 && Time.now.min == 0 ## se llama una vez ya q la simulación es cada 3 minutos
  			increase_players_credit(@players)
  		end

  		new_round = Round.new(game_id: @game.id)
  		new_round.save
  		@players_bets = Hash.new()
  		
  		@players.each do |player|
  			color_bet = player.getRandomColor()
  			color_obj_bet = Box.where(color: color_bet).first
  			bet_amount = player.do_bet()
	  		new_bet = Bet.new(round_id: new_round.id, player_id: player.id, box_id: color_obj_bet.id, amount: bet_amount)
	  		new_bet.save
	  		
	  		@bets << new_bet
	  		@players_bets[player] = new_bet.id
	  	end

	  	@winner_color = @BOARD.spin_roulette()
	  	update_players_credit(@players_bets,@winner_color)

	  	new_round.result = @winner_color
	  	new_round.save

	  	@rounds << new_round
  		# sleep(1) ## 60*3 sleep for 3 minutes
  	# end

  end

  def update_players_credit(players_bets,bet_result_color)
  	players_bets.each do |player_bet|
  		player_bet[0].update_credit_after_result(player_bet[1],bet_result_color)
  	end
  end

  def increase_players_credit(players)
  	players.each do |player|
  		player.increase_credit
  	end
  end


end
