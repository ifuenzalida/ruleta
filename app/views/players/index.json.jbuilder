json.array!(@players) do |player|
  json.extract! player, :id, :user_name, :credit
  json.url player_url(player, format: :json)
end
