class Game < ActiveRecord::Base
	has_one :board
	has_many :matches
	has_many :players, through: :matches
	has_many :rounds
end
