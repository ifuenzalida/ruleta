class Player < ActiveRecord::Base
  include PlayersHelper
	has_many :matches
	has_many :games, through: :matches
	has_many :bets

	validates :user_name, uniqueness: true
	before_save :default_values

	def default_values
  	self.credit ||= 10000
	end

	def increase_credit
		self.credit += 10000
		self.save
	end

  def getRandomColor
    rand_num = rand(100)
    color = "verde"
    if rand_num < 49
      color = "blanco"
    elsif rand_num >= 49 && rand_num < 98
      color = "negro"
    end
    return color  
  end

  def do_bet
    rain = get_rain_forcast()
    credit_fraction_to_bet = get_credit_fraction(rain)
    bet_amount = self.credit * credit_fraction_to_bet
    self.credit *= (1 - credit_fraction_to_bet)
    self.save

    return bet_amount
  end

  def get_credit_fraction(rain)
    credit_fraction_to_bet = 0
    if self.credit == 0
      credit_fraction_to_bet = 0
    elsif self.credit <= 1000
      credit_fraction_to_bet = 1
    elsif rain
      credit_fraction_to_bet = (4 + rand(7)).to_f / 100
    else
      credit_fraction_to_bet = (8 + rand(8)).to_f / 100
    end 

    return credit_fraction_to_bet
  end

  def update_credit_after_result(bet_id,bet_result_color)
    bet = Bet.find(bet_id)
    box_color = Box.find(bet.box_id)
    
    if box_color.color == bet_result_color
      prize = bet.amount * box_color.payoff
      self.credit += prize
      self.save
    end

  end

end
