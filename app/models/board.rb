class Board < ActiveRecord::Base
	has_many :boxes, dependent: :destroy
	belongs_to :game

	def spin_roulette()
  		rand_num = rand(100)
	  	color = "verde"
	  	if rand_num < 49
	  		color = "blanco"
	  	elsif rand_num >= 49 && rand_num < 98
	  		color = "negro"
	  	end
	  	return color	
  	end

end
