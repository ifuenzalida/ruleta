class Box < ActiveRecord::Base
	validates_uniqueness_of :id
  	validates_presence_of :board_id

	belongs_to :board
	has_many :bets
end
