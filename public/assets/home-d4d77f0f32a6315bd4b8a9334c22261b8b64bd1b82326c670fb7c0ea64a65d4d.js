/* global  jQuery, $*/

'use strict';
jQuery(function(){  

  $('#stop_simul').on('click',function(event) { 
  	continuar = false;
  });

  $('#start_simul').on('click',function(event) { 
  	continuar = true;
  	var k = 1;
  	var game_id = $('#ids_game').val();
  	var player_ids = [];
  	var players_checked = $('.check:checked');

  	if(players_checked.length < 1){
  		alert("Tienes que seleccionar al menos 1 jugador!");
  	}
  	else{

	  	for(var i = 0; i < players_checked.length; i++){
	  		player_ids.push($(players_checked[i]).attr('id'));
	  	}

		  $.ajax({
		    url: 'simulation_table',
		    data: { game_id: game_id, player_ids: player_ids}
		  }).success(function() {
		  		run_simulation(game_id, player_ids, k);
		  });
		  
		}

  });


             

	function run_simulation(game_id, player_ids, round_index){
	   fill_table(game_id, player_ids, round_index);

	   setTimeout(function() {  
	      round_index++;                   
	      if(continuar) {          
	         run_simulation(game_id, player_ids, round_index);           
	      }                      
	   }, 1000*60*3) // 1 segundo = 1000
	}

	function fill_table(game_id, player_ids, round_index){
		$.ajax({
		    url: 'simulation',
		    data: { game_id: game_id, player_ids: player_ids, round_num: round_index}
		}).success(function() { });
	}

});
